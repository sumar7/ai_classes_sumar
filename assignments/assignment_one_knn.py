# Coded by Sajjad Umar
# NIAIS - 983 AI Section A

import numpy as np
import pandas as pd

col_names = ['pregnant', 'glucose', 'bp', 'skin', 'insulin', 'bmi', 'pedigree', 'age', 'label']

df = pd.read_csv("../assets/datasets/diabetes.csv", header=None, names=col_names)

with_zero = ['glucose', 'bp', 'skin', 'bmi', 'insulin']

# replacing zero with mean
for column in with_zero:
    df[column] = df[column].replace(0, np.NaN)
    mean = int(df[column].mean(skipna=True))
    df[column] = df[column].replace(np.NaN, mean)

inputs = df.iloc[:, 0:8]
targets = df.iloc[:, 8]

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(inputs, targets, test_size=0.20, random_state=0)

from sklearn.preprocessing import StandardScaler

sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)

from sklearn.neighbors import KNeighborsClassifier

KNNclf = KNeighborsClassifier(n_neighbors=11, metric='euclidean')
KNNclf.fit(X_train, y_train)


def predict():
    predicted_value.set('calculating')
    try:
        entered_values = [[eval(entries[0].get()),
                           eval(entries[1].get()),
                           eval(entries[2].get()),
                           eval(entries[3].get()),
                           eval(entries[4].get()),
                           eval(entries[5].get()),
                           eval(entries[6].get()),
                           eval(entries[7].get())]]
        predicted_value.set(str(KNNclf.predict(entered_values)[0]))
    except:
        predicted_value.set('Invalid input')


from sklearn.metrics import accuracy_score


def accuracy():
    calculated_accuracy.set('calculating')
    y_pred = KNNclf.predict(X_test)
    calculated_accuracy.set(accuracy_score(y_test, y_pred))


from sklearn.metrics import confusion_matrix


def confusion():
    calculated_confusion.set('calculating')
    y_pred = KNNclf.predict(X_test)
    calculated_confusion.set(confusion_matrix(y_test, y_pred))


from tkinter import *

window = Tk()
window.geometry('560x380')
window.title('Diabetes Prediction')

index = 0
entries = []
for name in col_names[0:-1]:
    Label(window, text=f'Enter {name}: ').grid(row=index, column=0)
    entry = Entry(window, width=30)
    entry.grid(row=index, column=1)
    entries.append(entry)
    index += 1

Button(window, text='Predict Diabetes', command=predict).grid(row=8, column=0)
Button(window, text='Calculate Accuracy', command=accuracy).grid(row=9, column=0)
Button(window, text='Calculate Confusion Matrix', command=confusion).grid(row=10, column=0)

predicted_value = StringVar(value="Yet to find...")
calculated_accuracy = StringVar(value="Yet to find...")
calculated_confusion = StringVar(value="Yet to find...")
Label(window, textvariable=predicted_value).grid(row=8, column=1)
Label(window, textvariable=calculated_accuracy).grid(row=9, column=1)
Label(window, textvariable=calculated_confusion).grid(row=10, column=1)

window.mainloop()
