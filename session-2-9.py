# NLP, download NL tool kit
# import nltk
# nltk.download()

# testing the NL took kit setup
from nltk.corpus import brown

print(brown.words)

print(brown.categories())
print(len(brown.categories()))

review_words = brown.words(categories='reviews')
print(review_words)
print(len(review_words))

# tokenizing
from nltk.tokenize import word_tokenize, sent_tokenize

EXAMPLE_TEXT = """A Quick brown fox jumps over the lazy dog.
this is an example text to test tokenize"""

print(word_tokenize(EXAMPLE_TEXT))

print(sent_tokenize(EXAMPLE_TEXT))

# stop words of any language, stopwords has no contribution in sentimental analysis
from nltk.corpus import stopwords

stop_words = set(stopwords.words('english'))
print(stop_words)
print(len(stop_words))

word_tokens = word_tokenize(EXAMPLE_TEXT)
filtered_sentence = [w for w in word_tokens if w not in stop_words]
all_stopwords_in_sentence = [w for w in word_tokens if w in stop_words]
print(f'This is filtered: {filtered_sentence}')
print(f'All stopwords in sentence: {all_stopwords_in_sentence}')

# stemming

from nltk.stem import PorterStemmer

ps = PorterStemmer()
example_words = ['python', 'pythoner', 'pythoning', 'pythonly', 'pythoned']

new_text = 'It is very important to be very pythonly while you are pythoning ' \
           'All pythoners have pythoned poorly at least once'
words = word_tokenize(new_text)
sent = sent_tokenize(new_text)

print('The stemmed words are')
for w in example_words:
    print(ps.stem(w))



