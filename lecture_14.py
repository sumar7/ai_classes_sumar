# Introduction to Image processing with Open CV

import cv2 as cv
from matplotlib import pyplot as plt

# read image file from assets
img = cv.imread('assets/images/sample-image.png', cv.IMREAD_COLOR) # other options are IMREAD_GRAYSCALE or
# IMREAD_UNCHANGED

print(img)

# changing image size
cv.namedWindow('Test Image Name', cv.WINDOW_FULLSCREEN)

# Showing image, window name and image name should be same to apply effects
cv.imshow('Test Image Name', img)
key = cv.waitKey(0) # waiting for the key press 0 will wait for key press 5000 will automatically destroy window after 5
# sec

# save image to new file or destroy window
if key == 27: # ASCII code for ESC
    cv.destroyAllWindows()
elif key == ord('s'): # ASCII code of letter 's'
    # writing file
    cv.imwrite('assets/images/sample-write-file.jpg', img)
    cv.destroyAllWindows()

# using mat-plot lib
# MAT-PLOT show in RGB mode while Open CV shows in BGR that is why their is a color difference

plt.imshow(img)
plt.xticks([])
plt.yticks([])
plt.show()


