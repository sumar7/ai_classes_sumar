import pymysql

# connect to database
db = pymysql.connect('localhost', 'databases', '1234', 'demo')

# get the cursor object
cursor = db.cursor()

# printing select version
print(cursor.execute('SELECT VERSION()'))

# printing current version of my_sql
print(f'You are running version {cursor.fetchone()}')

# Dropping table if exists
cursor.execute('DROP TABLE IF EXISTS student')

# writing raw query to create table student
query = """CREATE TABLE student(first_name VARCHAR(20), last_name VARCHAR(20),
age INT, enrolment_no VARCHAR(12))"""

# executing query
print(cursor.execute(query))

# inserting data into table student
insert_query = """INSERT INTO student VALUES('Sajjad', 'Umar', 21, '982')"""

# executing query with exception handling
try:
    cursor.execute(insert_query)
    db.commit()
except:
    db.rollback()

fetch_query = """select * from student where age>=20"""

try:
    cursor.execute(fetch_query)
    result = cursor.fetchall()
    for record in result:
        first_name = record[0]
        last_name = record[1]
        age = record[2]
        enrolment_number = record[3]
        print(f'First Name: {first_name}\n Last Name: {last_name}\n Age: {age}\n Enrolment Number: {enrolment_number}')
except:
    print('something went wrong.')

# closing database connection
db.close()
