# visualization of data with mat-plot lib or cyborg

# importing mat-plot sub module (class) with nickname
import matplotlib.pyplot as plt
import math
import numpy as np
import datetime

# using mat-plot we are drawing a straight line in graph
plt.plot([1, 2, 3, 4])

# drawing another line with different data
plt.plot([2, 7, 4, 2, 9])

# drawing using x axis array and y axis array
x = [1, 2, 3, 4]
y = [5, 4, 6, 7]

# assigning title
plt.title('My First Plot')

# assigning label to axis
plt.xlabel('x axis', color='green')
plt.ylabel('y axis', color='orange')

# adding text at specific coordinates
plt.text(1, 1.5, 'First Text', color='red')
plt.text(2, 3.5, 'Second Text', color='green')

# adding grid
plt.grid(True)

# adding legend
plt.legend(['First Series', 'Second Series'], loc=1)  # 0, 1, 2, 3 connors

# assigning limits to axis (for some reason it is not working check on google)
x_min = 0
x_max = 5
y_min = 0
y_max = 20
plt.axis([x_min, x_max, y_min, y_max])

# ro for red dots (bo for blue) rs and bs for squares, r^ or b^ for triangle (use -- for doted line)
plt.plot(x, y, 'g*--')  # -. is for dash and doted line, g*-- for green ascetics

# showing the chart
plt.show()

# using math lib, finding total possible values with specific jump with range
min_value = 0
max_value = 2.5
jump = 0.1
x_values = np.arange(min_value, max_value, jump)
print(len(x_values))

print(math.pi)  # 3.14

# find sin of values in x_values
y1 = np.sin(math.pi * x_values)
y2 = np.sin(math.pi * x_values + math.pi / 4)
y3 = np.sin(math.pi * x_values - math.pi / 4)
print(y1)
print(y2)
print(y3)
plt.plot(x_values, y1, 'b*', x_values, y2, 'g^', x_values, y3, 'ys')
plt.show()

plt.subplot(211)  # 2 rows 1st column 1st plot
plt.plot(x_values, y1, 'b*', x_values, y2, 'g^', x_values, y3, 'ys')
plt.subplot(212)  # 2 rows 2nd column 1st plot
plt.plot(x_values, y1, 'b--', x_values, y2, 'g--', x_values, y3, 'y-.', linewidth=2)
plt.show()

plt.subplot(121)  # 1 row 2 columns 1st plot
plt.plot(x_values, y1, 'b*', x_values, y2, 'g^', x_values, y3, 'ys')
plt.subplot(122)  # 1 rows 2 columns 1st plot
plt.plot(x_values, y1, 'b--', x_values, y2, 'g--', x_values, y3, 'y-.', linewidth=2)
plt.show()

plt.title('Date Graph')
events = [datetime.date(2020, 1, 4), datetime.date(2020, 1, 5), datetime.date(2020, 1, 6),
          datetime.date(2020, 1, 7), datetime.date(2020, 1, 8), datetime.date(2020, 1, 9)]
readings = [1, 2, 3, 4, 5, 6]
plt.plot(events, readings, 'ro-.')
plt.grid(True)
plt.show()

# generating histogram
min_bound = 0
max_bound = 100
values_amount = 100
random_numbers = np.random.randint(min_bound, max_bound, values_amount)
plt.hist(random_numbers, bins=20)
plt.show()

# bar chart
index = [1, 2, 3, 4, 5]
values = [6, 3, 2, 4, 1]
plt.bar(index, values)  # use barh for horizontal bar chart
plt.xticks(index, ['A', 'B', 'C', 'D', 'E'])
plt.yticks(index, ['A', 'B', 'C', 'D', 'E'])
plt.show()

# horizontal bar chart
plt.barh(index, values)  # use barh for horizontal bar chart
# plt.xticks(index, ['A', 'B', 'C', 'D', 'E'])
plt.yticks(index, ['A', 'B', 'C', 'D', 'E'])
plt.show()

# pie chart (details and styling on slides)
labels = ['Nokia', 'Samsung']
values = [40, 60]
colors = ['yellow', 'red']
explode = [0.2, 0]
plt.title('pie chart')
plt.pie(values, labels=labels, colors=colors, explode=explode, startangle=180)
plt.show()

# piloting charts using pandas lib (install panda first) on slides
