# video processing
import cv2 as cv

# Capture video using camera
# capture = cv.VideoCapture(0)  # 0 is a camera index

# Capturing video from assets
capture = cv.VideoCapture('assets/videos/sample-screencast.mp4')

fourcc = cv.VideoWriter.fourcc(*'XVID') # codec mode
output = cv.VideoWriter('assets/videos/sample-output.avi', fourcc, 20.0, (640, 480))

if not capture.isOpened():
    print('Can not open camera')
    exit()

while True:
    receiving, frame = capture.read()  # capture frame by frame

    if not receiving: # receiving is a boolean returning True or False
        print('Can not receive frame (stream end?). Exiting...')
        break

    # Our operations on the frame come here
    gray = cv.cvtColor(frame, cv.COLOR_RGB2BGR)

    output.write(frame) # frame can be flipped using cv.flip(frame, 0)

    # displaying the resulting frame with color filter
    cv.imshow('Gray Frame', gray)

    # cv.imshow('Original Frame', frame) # uncomment for without color converter

    if cv.waitKey(1) == ord('q'):  # frame lag and speed
        break

# when everything done, release the capture and output
capture.release()
output.release()
cv.destroyAllWindows()
