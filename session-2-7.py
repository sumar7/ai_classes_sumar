import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.express as px

import warnings

warnings.filterwarnings('ignore')

df = pd.read_csv('assets/datasets/covid_19_clean_complete.csv', parse_dates=['Date'])
print(df.head())

df.rename(columns={'Date': 'date', 'Province/State': 'state', 'Country/Region': 'country',
                   'Lat': 'lat', 'Long': 'long', 'Confirmed': 'confirmed', 'Deaths': 'deaths',
                   'Recovered': 'recovered'}, inplace=True)

print(df.head())

df['active'] = df['confirmed'] - (df['deaths'] + df['recovered'])

top = df[df['date'] == df['date'].max()]

world = top.groupby('country')['confirmed', 'active', 'deaths'].sum().reset_index()
print(world.head())

figure = px.choropleth(world, locations='country', locationmode='country names', color='active', hover_name='country',
                       range_color=[1, 1000],
                       color_continuous_scale='peach', title='countries with active cases')
figure.show()

plt.figure(figsize=(15, 10))
plt.xticks(rotation=90, fontsize=10)
plt.yticks(fontsize=10)
plt.xlabel('Dates', fontsize=20)
plt.title('World Wide Confirmed COVID-19 cases over time', fontsize=20)
plt.grid(True)
total_cases = df.groupby('date')['date', 'confirmed'].sum().reset_index()
total_cases['date'] = pd.to_datetime(total_cases['date'])

ax = sns.pointplot(x=total_cases.date.dt.date, y=total_cases.confirmed, color='r')
ax.set(xLabel='Date', yLabel='total cases')
plt.show()

top_actives = top.groupby(by='country')['active'].sum().sort_values(ascending=False).head(20).reset_index()

plt.figure(figsize=(15, 10))
plt.xticks(fontsize=10)
plt.yticks(fontsize=10)
plt.xlabel('Total Cases', fontsize=20)
plt.ylabel('Countries', fontsize=20)
plt.title('Top 20 countries having most cases', fontsize=20)

bx = sns.barplot(x=top_actives.active, y=top_actives.country)

for i, (value, name) in enumerate(zip(top_actives.active, top_actives.country)):
    bx.text(value, i - .05, f'{value:,.0f}', size=10, ha='left', va='center')

bx.set(xlabel='Total Cases', ylabel='Country')
plt.show()

top_deaths = top.groupby(by='country')['deaths'].sum().sort_values(ascending=False).head(20).reset_index()

plt.figure(figsize=(15, 10))
plt.xticks(fontsize=10)
plt.yticks(fontsize=10)
plt.xlabel('Total Deaths', fontsize=20)
plt.ylabel('Countries', fontsize=20)
plt.title('Top 20 countries having most Deaths', fontsize=20)

cx = sns.barplot(x=top_deaths.deaths, y=top_deaths.country)

for i, (value, name) in enumerate(zip(top_deaths.deaths, top_deaths.country)):
    cx.text(value, i - .05, f'{value:,.0f}', size=10, ha='left', va='center')

cx.set(xlabel='Total Deaths', ylabel='Country')
plt.show()

# similarly compare different countries (code provided in classroom)

pakistan = df[df.country == 'Pakistan'] # TODO: incomplete, please see code provided by Sir
