# support vector machine SML

from sklearn import datasets

cancer = datasets.load_breast_cancer()

print('Features: ', cancer.feature_names)
print('Labels: ', cancer.target_names)
print('Shape: ', cancer.data.shape)

X = cancer.data  # inputs
y = cancer.target  # targets ,labels

print(X[0:5])  # print first 5 records
print(y[0:5])  # print first 5 records (0, 1)

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)

from sklearn import svm

classifier = svm.SVC(kernel='linear')  # using linear kernel view documentation for other type of kernel

classifier.fit(X_train, y_train)

y_prediction = classifier.predict(X_test)

print(y_prediction)

from sklearn import metrics

accuracy = round(metrics.accuracy_score(y_test, y_prediction) * 100, 2)
print(f"Accuracy {accuracy}%")  # try changing kernel for better results (rbf, polly)

precession = round(metrics.precision_score(y_test, y_prediction) * 100, 2)
print(f"precession: {precession}")

recall = round(metrics.recall_score(y_test, y_prediction) * 100, 2)
print(f"recall: {recall}")

F1Score = round(metrics.f1_score(y_test, y_prediction) * 100, 2)
print(f"F1 score: {F1Score}")

print(metrics.classification_report(y_test, y_prediction))


