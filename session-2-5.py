# Random Forest Algorithm

from sklearn.datasets import load_digits

digits = load_digits()

print(dir(digits))

import matplotlib.pyplot as plt

# for i in range(4):
#     plt.matshow(digits.images[i])
#     plt.gray()
#
# plt.show()

import pandas as pd

df = pd.DataFrame(digits.data)
print(df.head())

df['target'] = digits.target
print(df[0:3])

X = df.drop('target', axis='columns')
print(X[0:3])
y = df.target
print(y[0:12])

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

from sklearn.ensemble import RandomForestClassifier

model = RandomForestClassifier(n_estimators=100)

model.fit(X_train, y_train)

score = model.score(X_test, y_test)

print(f"score: {score}")  # accuracy

y_predictions = model.predict(X_test)

from sklearn import metrics

cnf_metrics = metrics.confusion_matrix(y_test, y_predictions)
print(cnf_metrics)

import seaborn as sn
plt.figure()
sn.heatmap(cnf_metrics, annot=True)
plt.xlabel('Predicted Values')
plt.ylabel('Truth')
plt.show()

report = metrics.classification_report(y_test, y_predictions, digits=10)
print(report)