import numpy as np
import pandas as pd

col_names = ['pregnant', 'glucose', 'bp', 'skin', 'insulin', 'bmi', 'pedigree', 'age', 'label']

df = pd.read_csv("assets/datasets/diabetes.csv", header=None, names=col_names)
# print(df.head())
# print(df.describe())
# print(len(df))
# print(df.info())

with_zero = ['glucose', 'bp', 'skin', 'bmi', 'insulin']

for column in with_zero:
    df[column] = df[column].replace(0, np.NaN)
    mean = int(df[column].mean(skipna=True))
    df[column] = df[column].replace(np.NaN, mean)
print(df)
X = df.iloc[:, 0:8]  # Features
y = df.iloc[:, 8]  # Target variable
# print(X)
# print(y)

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=0)

from sklearn.preprocessing import StandardScaler

sc_X = StandardScaler()
X_train = sc_X.fit_transform(X_train)
X_test = sc_X.transform(X_test)

from sklearn.neighbors import KNeighborsClassifier

KNNclf = KNeighborsClassifier(n_neighbors=11, metric='euclidean')
KNNclf.fit(X_train, y_train)
y_pred = KNNclf.predict(X_test)

from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix

cm = confusion_matrix(y_test, y_pred)
print(cm)
print(accuracy_score(y_test, y_pred))

y_pred = KNNclf.predict(X_test)

from sklearn import metrics
import matplotlib.pyplot as plt
import seaborn as sns

cnf_matrix = metrics.confusion_matrix(y_test, y_pred)
sns.heatmap(pd.DataFrame(cnf_matrix), annot=True, cmap="YlGnBu")
plt.title('Confusion matrix', y=1.1)
plt.ylabel('Actual label')
plt.xlabel('Predicted label')
plt.show()

# from sklearn import neighbors
# from sklearn.metrics import f1_score,confusion_matrix,roc_auc_score
# f1_list=[]
# k_list=[]
# for k in range(1,50):
#     clf=neighbors.KNeighborsClassifier(n_neighbors=k,n_jobs=-1)
#     clf.fit(X_train,y_train)
#     pred=clf.predict(X_test)
#     f=f1_score(y_test,pred,average='macro')
#     f1_list.append(f)
#     k_list.append(k)

# best_f1_score=max(f1_list)
# best_k=k_list[f1_list.index(best_f1_score)]
# print("Optimum K value=",best_k," with F1-Score=",best_f1_score)
