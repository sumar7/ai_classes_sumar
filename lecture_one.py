a = 5
b = "Hello World"
if 5 < 10:
    print(b)

print('%s, %i is my lucky number' % (b, a))

print("{0} dummy {1} text".format(a, b))

print("{a} dummy text".format(a=5))

print(f"{a} dummy text {b}")
