from tkinter import *
import pandas as pd  # dataset read write
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression

win = Tk()

win.geometry('480x360')

win.title('Welcome to NIAIS Training program')

label = Label(win, text='Enter your name', font=('Arial Bold', 16))
label.grid(column=0, row=0)


def clicked():
    label.configure(text='button was clicked')


btn = Button(win, text='Click Me', font=('Areal Bold', 16), command=clicked)
btn.grid(column=1, row=0)

text = Entry(win, width=10)
text.focus()
text.grid(column=1, row=0)

# to get text
print(text.get())  # on click assign this text to label

win.mainloop()

# using tkinter GUI lib for Liner Regression
# --------------------------------------------

df = pd.read_csv('assets/datasets/salary_data.csv')

# getting inputs and targets
inputs = df.iloc[:, :-1].values  # get a copy of inputs : means all rows, :-1 means all columns except last
targets = df.iloc[:, 1].values  # get a copy of targets all rows and column at index 1

# splitting the data set into the set of train and test
x_train, x_test, y_train, y_test = train_test_split(inputs, targets, test_size=0.3, train_size=0.7, random_state=0)

LR = LinearRegression()
LR.fit(x_train, y_train)

print(f'linear score - accuracy (%): {LR.score(x_test, y_test) * 100}')  # y = m * x + c, where m is slope and c is

window = Tk()
window.geometry('360x280')
window.title('Experienced Salary Prediction System')


def predict():
    out.delete(0, END)
    acc.delete(0, END)
    d1 = year.get()
    f = int(d1)
    print(f)
    y_pred = LR.predict([[f]])
    year.delete(0, END)

    out.insert(0, float(y_pred))


def accuracy():
    out.delete(0, END)
    acc.delete(0, END)
    result = LR.score(x_test, y_test)
    acc.insert(0, float(result * 100))


Label(window, text='Enter the experience in years: ').grid(row=0, column=0)
year = Entry(window, width=30)
year.grid(row=0, column=1)
year.focus()

acc = Entry(window, width=30)
acc.grid(row=1, column=1)

out = Entry(window, width=30)
out.grid(row=2, column=1)

Button(window, text='Test Model Accuracy', command=accuracy).grid(row=1, column=0)
Button(window, text='Prediction', command=predict).grid(row=2, column=0)

window.mainloop()

# assignment
# combo box, menu bar prepaid a calculator and notepad application
