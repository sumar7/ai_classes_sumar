# Logical Regression

import pandas as pd

col_names = ['pregnant', 'glucose', 'bp', 'skin', 'insulin', 'bmi', 'pedigree', 'age', 'label']

df = pd.read_csv("assets/datasets/diabetes.csv", header=None, names=col_names)
print(df.head())

feature_columns = ['pregnant', 'insulin', 'bmi', 'age', 'glucose', 'bp', 'pedigree']

features = df[feature_columns]  # Features
targets = df.label  # target variables

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(features, targets, test_size=0.25, random_state=0)

from sklearn.linear_model import LogisticRegression

log_reg = LogisticRegression(max_iter=200)
log_reg.fit(X_train, y_train)  # training the algorithm
y_predictions = log_reg.predict(X_test)

print(y_predictions)

from sklearn import metrics

cnf_metrics = metrics.confusion_matrix(y_test, y_predictions)
print(cnf_metrics)

import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

class_name = [0, 1]  # name of classes
fig, ax = plt.subplots()

tick_marks = np.arange(len(class_name))
plt.xticks(tick_marks, class_name)
plt.yticks(tick_marks, class_name)
plt.title('Confusion Metrics')
plt.ylabel('Actual Label')
plt.xlabel('Predicted Label')


sns.heatmap(pd.DataFrame(cnf_metrics), annot=True, cmap='YlGnBu', fmt='g')
plt.show()

print('Accuracy: ', metrics.accuracy_score(y_test, y_predictions) * 100)
print('Precision: ', metrics.precision_score(y_test, y_predictions) * 100)
print('Recall: ', metrics.recall_score(y_test, y_predictions) * 100)

