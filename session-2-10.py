# NLP, Parts of speech

import nltk
from nltk.corpus import state_union
from nltk.tokenize import PunktSentenceTokenizer, sent_tokenize

# test train data
train_text = state_union.raw("/home/sajjad/PycharmProjects/ai_classes_sumar/assets/datasets/text_speech_one.txt")

example = "Quick brown fox jumps over the lazy dog." \
          "This is paragraph speech by Bush" \
          "He said you are 14 years old"

custom_sent_tokenizer = PunktSentenceTokenizer(example)
tokenized = custom_sent_tokenizer.tokenize(example)
sentence_t = sent_tokenize(example)


def process_content():
    try:
        for i in tokenized:
            words = nltk.word_tokenize(i)
            tagged = nltk.pos_tag(words)
            print(tagged)
    except Exception as e:
        print(str(e))


process_content()

# NLP WordNetLemmatizer (convert slang to original words)

from nltk.stem import WordNetLemmatizer

lemmatizer = WordNetLemmatizer()

print(lemmatizer.lemmatize("cats", pos='n'))  # n means noun
print(lemmatizer.lemmatize("cacti", pos='n'))
print(lemmatizer.lemmatize("geese"))
print(lemmatizer.lemmatize("rocks"))
print(lemmatizer.lemmatize("python"))
print(lemmatizer.lemmatize("better", pos='a'))  # a means adjective
print(lemmatizer.lemmatize("best", pos='a'))
print(lemmatizer.lemmatize("run", pos='a'))
print(lemmatizer.lemmatize("ran", pos='v'))
print(lemmatizer.lemmatize("Caring", pos='a'))

sentence = "The striped bats are hanging on their feet for, the quick brown fox jumps over the lazy dog"

# tokanize to split into words

word_list = nltk.word_tokenize(sentence)
print(word_list)

# now lemmatize the sentence, join words with space (' ') to form a sentence
lemmatized_output = ' '.join([lemmatizer.lemmatize(w) for w in word_list])
print(lemmatized_output)

# NLP corpora

from nltk.corpus import gutenberg

sample = gutenberg.raw('bible-kjv.txt')
tok = sent_tokenize(sample)

# print(tok)

for x in range(5, 10):
    print(f'{x} : {tok[x]}')

# NLP, word net

from nltk.corpus import wordnet

syns = wordnet.synsets('program')

print(syns[0])
print('the synonym is :', syns[0].name(), '\n')
print('the synonym is :', syns[0].lemmas()[0].name(), '\n')
print('the synonym is :', syns[0].definition(), '\n')
print('the synonym is :', syns[0].examples(), '\n')

synonyms = []
antonyms = []

a = input('Enter any English word')

for syn in wordnet.synsets(a):
    for m in syn.lemmas():
        print('L: ', m)

        synonyms.append(m.name())
        if m.antonyms():
            antonyms.append(m.antonyms()[0].name())

print(f'the synonyms of {a} are: {set(synonyms)}')
print(f'the antonyms of {a} are: {set(antonyms)}')

# comparison

w1 = wordnet.synset('ship.n.01')
w2 = wordnet.synset('boat.n.01')

print('The similarity of ship and boat is : ', w1.wup_similarity(w2) * 100)

w1 = wordnet.synset('ship.n.01')
w2 = wordnet.synset('cat.n.01')
print('The similarity of ship and cat is : ', w1.wup_similarity(w2) * 100)

w1 = wordnet.synset('ship.n.01')
w2 = wordnet.synset('car.n.01')
print('The similarity of ship and car is : ', w1.wup_similarity(w2) * 100)