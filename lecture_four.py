# Functions & Object Oriented Programming


# FUNCTIONS IN PYTHON.


def some_function():
    my_name = 'sumar'
    print('running some function')
    print(f"my name is {my_name}")


def some_function_with_input():
    print('running some function with input')
    my_name = input("Enter your name: ")
    print(f"my name is {my_name}")


# functions to calculate sum of two numbers
def sum_two_numbers(number_one, number_two):
    print(f"Sum of number one & number two is = {number_one + number_two}")


def sum_by_tacking_input_from_user():
    print(
        '\nrunning sum by taking input from user, eval helps you to add both int & float but you can not enter string')
    number_one = eval(input('Enter first number: '))
    number_two = eval(input('Enter second number: '))
    sum_two_numbers(number_one, number_two)


def is_number_even(number_one):
    if number_one % 2 == 0:
        return True
    else:
        return False


def is_number_even_short_form(number_one):
    return number_one % 2 == 0


variable_with_global_scope = 10


def print_global_variable():
    # you can not modify global variable in local scope, to modify that you need call it using global keyword
    global variable_with_global_scope
    print(f"global variable is: {variable_with_global_scope}")
    # modifying global variables
    variable_with_global_scope += 5
    print(f"modified global variable is: {variable_with_global_scope}")


my_variable_with_lambda = lambda a, b: (a * b) + 2


# recursive calling to find factorial
def find_factorial(input_number):
    if input_number == 1 or input_number == 0:
        return 1
    return input_number * find_factorial(input_number - 1)


# Calling functions
some_function()
some_function_with_input()
sum_two_numbers(5, 5)
sum_by_tacking_input_from_user()
print(f"find if number '5' is even: {is_number_even(5)}")
print(f"find if number '4' is even: {is_number_even_short_form(4)}")
print_global_variable()
# printing global variable, note now global variable has new value
print(f"printing global variable: {variable_with_global_scope}")
print(f"printing variable using lambda function: {my_variable_with_lambda(5, 10)}")
print(f"Factorial of number '10' = {find_factorial(10)}")


# CLASSES IN PYTHON

class Fruit:
    """this class create instance of fruits"""
    color = ''
    type = ''

    def print_apple(self):
        print(f"color of apple is {self.color}")
        print(f"type of apple is {self.type}")

    def function_inside_function(self):
        print(f"printing apple color from outer function: {self.color}")

        def my_inner_function():
            print('this is inner function')

        my_inner_function()


apple = Fruit()
apple.color = 'red'
apple.type = 'big apple'
apple.print_apple()
apple.function_inside_function()
