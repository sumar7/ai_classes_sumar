import numpy as np  # number calculations
import matplotlib.pyplot as plt  # graph plot lib
import pandas as pd  # dataset read write
import seaborn as sns  # also a graph plot library

df = pd.read_csv('assets/datasets/salary_data.csv')

print(df.info())  # get file information
print(df)  # print complete data set
print(df.sample(5))  # pick random 5 entries
print(df.head(3))  # pick first 3 entries
print(df.tail(3))  # pick last 3 entries

# plot-ing data using mat plot lib
plt.figure()
plt.xlabel('Years Experience', fontsize=16, color='g')
plt.ylabel('Salary', fontsize=16, color='g')
plt.title('Salary of Employee\n with Experience', fontsize=16, color='g')
plt.scatter(df.YearsExperience, df.Salary, color='red', marker='*')
plt.axis([0, 12, 20000, 140000])
plt.grid(True)
plt.show()

# getting inputs and targets
inputs = df.iloc[:, :-1].values  # get a copy of inputs : means all rows, :-1 means all columns except last
targets = df.iloc[:, 1].values  # get a copy of targets all rows and column at index 1
print(inputs)
print(targets)

# splitting the data set into the set of train and test
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(inputs, targets, test_size=0.3, train_size=0.7, random_state=0)

print(f'x-train {x_train}')
print(f'y-train {y_train}')
print(f'x-test {x_test}')
print(f'y-test {y_test}')

from sklearn.linear_model import LinearRegression
LR = LinearRegression()
LR.fit(x_train, y_train)
print(f'linear score - accuracy (%): {LR.score(x_test, y_test) * 100}')  # y = m * x + c, where m is slope and c is
# interceptor, m is coefficient
print(LR.coef_)
print(LR.intercept_)

# visualizing the training set (predicted values)

plt.scatter(x_train, y_train, color='red')
plt.plot(x_train, LR.predict(x_train), color='blue', alpha=0.3)
plt.title('Salary VS Experience\n (Training Set)')
plt.xlabel('Years of Experience')
plt.ylabel('Salary')
plt.legend(['Predicted Values', 'Original Values'])
plt.show()

# visualizing the test set (predicted values)

plt.scatter(x_test, y_test, color='red')
plt.plot(x_train, LR.predict(x_train), color='green', alpha=0.3)
plt.title('Salary VS Experience\n (Test Set)')
plt.xlabel('Years of Experience')
plt.ylabel('Salary')
plt.legend(['Predicted Values', 'Original Values'])
plt.show()

# correlation
print(f'correlation: {df.corr()}')
sns.heatmap(df.corr(), annot=True, cmap='YlGnBu')  # YlGnBu yellow, green and blue
plt.show()

# prediction
salary_prediction = LR.predict([[15]]) # expected salary of 15 year experience
print(f'Expected Salary is: {salary_prediction}')

# comparing original and test set results
all_salary_predictions = LR.predict(x_test)
print(f'all test salary predictions: {all_salary_predictions}')

index = np.arange(9)
bar_width = 0.4
plt.ylim(20000, 140000)
plt.bar(index, y_test, bar_width, color='b', alpha=0.7)
plt.bar(index + bar_width, all_salary_predictions, bar_width, color='red')
plt.legend(['Actual Values', 'Predicted Values'])
plt.show()

g = sns.pairplot(df, kind='scatter')
sns.boxplot(x=df['Salary'])
plt.show()