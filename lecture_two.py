# This is AI lecture two 17-11-2019

import keyword

# introduction to variables
number = 20_000
print(number)  # variable must be initialized.

# arrays
my_number_array = [1,2,3,4,5]
print(my_number_array)

# array can contain data of multiple types.
multi_type_array = [1, 2, 3, 4, 'One', 'Two', 'Three', 'Four']
print(multi_type_array)

my_array = "hello world".split(' ')
print(my_array)


# delete value from index
del(my_number_array[0])
print(my_number_array)

# print length of array
print(f"array length is {len(my_number_array)}")

# use of keyword
print(keyword.kwlist)
print(f"there are {len(keyword.kwlist)} reserved keywords in python")

# using keyword as a variable will result in error.
# False = 10

# multiple variable assignment
age, city = 21, 'Lahore'
print(f"my age is {age}", f"and I live in {city}")

print(type(age)) # data type of variable.

# complex variables
complex_variable = 2 + 3j
print(complex_variable)
print(type(complex_variable))

# for loop
for x in keyword.kwlist:
    print(f"{x} is a keyword.")


