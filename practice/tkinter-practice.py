from tkinter import *

win = Tk()
win.geometry("480x480")
win.title('The Test Title')

# text inputs

label = Label(win, text='Enter your name', font=('Arial Bold', 16))
label.grid(column=0, row=0)

entry = Entry(win, width=20)
entry.grid(column=1, row=0)
entry.focus()


def clicked():
    result.configure(text=f"Your Name is {entry.get()}")


button = Button(win, text='Click Me', command=clicked)
button.grid(column=1, row=1)

result = Label(win).grid(column=1, row=2)

win.mainloop()
