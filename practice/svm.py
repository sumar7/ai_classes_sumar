from sklearn import datasets

cancer_data = datasets.load_breast_cancer()

print(cancer_data.data.shape)

X = cancer_data.data
y = cancer_data.target

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=1, test_size=0.30)

from sklearn import svm

clf = svm.SVC(kernel='linear')

clf.fit(X_train, y_train)

y_pred = clf.predict(X_test)

from sklearn import metrics

accuracy = round(metrics.accuracy_score(y_test, y_pred) * 100, 2)
print(accuracy)

precision = round(metrics.precision_score(y_test, y_pred) * 100, 2)
print(precision)

recall = round(metrics.recall_score(y_test, y_pred) * 100, 2)
print(recall)

f1_score = round(metrics.f1_score(y_test, y_pred) * 100, 2)
print(f1_score)

print(metrics.classification_report(y_test, y_pred))
