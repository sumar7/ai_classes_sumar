# random forest to recognise digits in in-built data sets

from sklearn.datasets import load_digits

digits = load_digits()

import matplotlib.pyplot as plt

# viewing loaded digits in grayscale

# for i in range(10):
#     plt.matshow(digits.images[i])
#     plt.gray()
# plt.show()

import pandas as pd

df = pd.DataFrame(digits.data)
df['target'] = digits.target # adding targets to data frame

# inputs and targets

X = df.drop('target', axis='columns')
y = df.target

# splitting the data into train and test models

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)

# initializing and training the random forest

from sklearn.ensemble import RandomForestClassifier

clf = RandomForestClassifier(n_estimators=100)
clf.fit(X_train, y_train)

# score

scr = clf.score(X_test, y_test)
print(scr)

# predicting

predictions = clf.predict(X_test)

# confusion matrix

from sklearn.metrics import confusion_matrix
import seaborn as sns

cm = confusion_matrix(y_test, predictions)
sns.heatmap(pd.DataFrame(cm), cmap='YlGnBu', annot=True)
plt.figure(figsize=(10, 7))
plt.title('CM')
plt.show()

from sklearn.metrics import classification_report

report = classification_report(y_test, predictions, digits=10)
print(report)


