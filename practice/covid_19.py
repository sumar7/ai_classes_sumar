import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.express as px

df = pd.read_csv('../assets/datasets/covid_19_clean_complete.csv', parse_dates=['Date'])
print(df.head())

df.rename(columns={'Date': 'date', 'Province/State': 'state', 'Country/Region': 'country', 'Lat': 'lat', 'Long': 'long',
                   'Confirmed': 'confirmed', 'Deaths': 'deaths',
                   'Recovered': 'recovered'}, inplace=True)

print(df.head())

df['active'] = df['confirmed'] - (df['deaths'] + df['recovered'])

top = df[df['date'] == df['date'].max()]

world = top.groupby('country')['confirmed', 'active', 'deaths'].sum().reset_index()
print(world)

figure = px.choropleth(world, locations='country', locationmode='country names', color='active', hover_name='country', range_color=[1, 1000],
                       color_continuous_scale='peach', title='countries with active cases')

# figure.show()

plt.figure(figsize=(15, 10))
plt.xticks(rotation=90, fontsize=10)
plt.yticks(fontsize=20)
plt.xlabel('Dates', fontsize=20)
plt.title('COVID 19 WW')
plt.grid(True)
total_cases = df.groupby('date')['date', 'confirmed'].sum().reset_index()
total_cases['date'] = pd.to_datetime(total_cases['date'])

ax = sns.pointplot(x=total_cases.date.dt.date, y=total_cases.confirmed, color='r')
ax.set(xLabel='Date', yLabel='Total Cases')
plt.show()
