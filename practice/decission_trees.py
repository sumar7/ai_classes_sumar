import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn import metrics
import numpy as np

# reading the dataset
col_names = ['pregnant', 'glucose', 'bp', 'skin', 'insulin', 'bmi', 'pedigree', 'age', 'label']
dataset = pd.read_csv("../assets/datasets/diabetes.csv", header=None, names=col_names)

cols_with_zeros = ['glucose', 'bp', 'skin', 'bmi', 'insulin']

for col in cols_with_zeros:
    dataset[col] = dataset[col].replace(0, np.NaN)
    mean = int(dataset[col].mean(skipna=True))
    dataset[col] = dataset[col].replace(np.NaN, mean)

#  setting up inputs and targets

X = dataset.iloc[:, 0:8]
y = dataset.iloc[:, 8]

# splitting the data into train & test
X_train,X_test,y_train,y_test=train_test_split(X, y, test_size=0.30, random_state=1)


# using the decision tree algorithm

from sklearn.tree import DecisionTreeClassifier

tree = DecisionTreeClassifier()
tree = tree.fit(X_train, y_train)

predictions = tree.predict(X_test)

score = metrics.accuracy_score(y_test, predictions)

import matplotlib.pyplot as plt
import seaborn as sns

confusion_matrix = metrics.confusion_matrix(y_test, predictions)
print(confusion_matrix)
sns.heatmap(pd.DataFrame(confusion_matrix), annot=True, cmap="YlGnBu")
plt.title('Confusion Matrix', y=1.1)
plt.ylabel('Actual label')
plt.xlabel('Predicted')
plt.show()

