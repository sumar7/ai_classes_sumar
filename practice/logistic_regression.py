import pandas as pd


col_names = ['pregnant', 'glucose', 'bp', 'skin', 'insulin', 'bmi', 'pedigree', 'age', 'label']
feature_columns = ['pregnant', 'insulin', 'bmi', 'age', 'glucose', 'bp', 'pedigree']

df = pd.read_csv('../assets/datasets/diabetes.csv', header=None, names=col_names)

from sklearn.model_selection import train_test_split

print(len(df))

X = df[feature_columns]
y = df.label

X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0, test_size=0.3)

from sklearn.linear_model import LogisticRegression

clf = LogisticRegression(max_iter=200)
clf = clf.fit(X_train, y_train)

prediction = clf.predict(X_test)

# confusion matricx
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

cm = confusion_matrix(y_test, prediction)

class_name = [0, 1]  # name of classes
fig, ax = plt.subplots()

tick_marks = np.arange(len(class_name))
plt.xticks(tick_marks, class_name)
plt.yticks(tick_marks, class_name)
plt.title('Confusion Metrics')
plt.ylabel('Actual Label')
plt.xlabel('Predicted Label')
sns.heatmap(pd.DataFrame(cm), annot=True, cmap="YlGnBu", fmt='g')
plt.show()

