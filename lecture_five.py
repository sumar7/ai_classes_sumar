# file management

# importing modules
import os

# using module
print(f"current working directory is: {os.getcwd()}")  # getting current working directory
print(f"response type of current working directory method is: {type(os.getcwd())}")  # getting cwd type.

# changing current working directory
os.chdir('/home/sajjad')
print(f"new current working directory is: {os.getcwd()}")  # getting current working directory
# listing directories
print(f"listing {os.getcwd()}'s content:")
print(os.listdir())

# creating folder in current directory
os.mkdir('test_folder_python')
print(f"listing {os.getcwd()}'s content:")
print(os.listdir())

# renaming folder
os.rename('test_folder_python', 'renamed_test_folder')
print(f"listing {os.getcwd()}'s content:")
print(os.listdir())

# removing empty folder (directory that contains content can not be removed this way)
os.rmdir('renamed_test_folder')
print(f"listing {os.getcwd()}'s content:")
print(os.listdir())

# joining paths
print(os.path.join('/home', 'sajjad'))

# splitting path
print(os.path.split(os.getcwd()))

# checking if directory exists
print(os.path.exists('/home/sajjad'))
print(os.path.exists('/home/sajjad/unknown folder'))

# checking if path is a directory, similarly there is method for file, named 'is_file()'
print(os.path.isdir('/home/sajjad'))
print(os.path.isfile('/home/sajjad'))

# recursively traversing directory
for roots, dirs, files in os.walk('/home/sajjad/Desktop'):
    print(roots, dirs, files)

# recursively traversing directory and counting number of dirs and files
for roots, dirs, files in os.walk('/home/sajjad/Desktop'):
    print(roots, f"number of files: {len(dirs)},", f"number of files: {len(files)}")

# creating, renaming and removing dirs
os.chdir('/home/sajjad')
os.mkdir('test')
print(os.listdir())
os.rename('test', 'Test')
print(os.listdir())
os.removedirs('Test')
print(os.listdir())

# algorithm to rename all files.
os.chdir('/home/sajjad/Downloads/PythonTemp')
i = 1
for file in os.listdir():
    src = file
    dst = f"TempImage-{str(i)}.jpg"
    os.rename(src, dst)
    i += 1

print(f"renamed files are: {os.listdir()}")

# revisited algorithm
for index, file in enumerate(os.listdir(), start=1):
    os.rename(file, f'RenamedImg-{index}.jpg')

print(f"renamed files are: {os.listdir()}")

os.chdir('/home/sajjad/Downloads/TestTemp')

# # Open File in python
text_file = open('test', 'r')  # read mode 'r', append mode 'a'
try:
    print(text_file.read(2))
    print(text_file.tell()) # tells how many characters has be read already
    print(text_file.read()) # reading rest of the file
    text_file.seek(0) # moving cursor to zero index (start of the file)
    print(text_file.read())
    text_file.seek(0)
    print(f"reading single line {text_file.readline()}")
    text_file.seek(0)
    print(f"reading all lines {text_file.readlines()}")
finally:
    text_file.close()

# reading through loop
for line in open('test'):
    print(line, end='')
