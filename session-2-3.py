# Decision Tree algorithm

import pandas as pd
from sklearn import metrics
import numpy as np

col_names = ['pregnant', 'glucose', 'bp', 'skin', 'insulin', 'bmi', 'pedigree', 'age', 'label']

df = pd.read_csv("assets/datasets/diabetes.csv", header=None, names=col_names)

with_zero = ['glucose', 'bp', 'skin', 'bmi', 'insulin']

# replacing zero with mean
for column in with_zero:
    df[column] = df[column].replace(0, np.NaN)
    mean = int(df[column].mean(skipna=True))
    df[column] = df[column].replace(np.NaN, mean)

inputs = df.iloc[:, 0:8]
targets = df.iloc[:, 8]

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(inputs, targets, test_size=0.20, random_state=0)

from sklearn.tree import DecisionTreeClassifier

classifier = DecisionTreeClassifier()

# Training
classifier = classifier.fit(X_train, y_train)

# Prediction
y_pred = classifier.predict(X_test)

print("Accuracy:", metrics.accuracy_score(y_test, y_pred))

import pickle  # to save the current model

with open('assets/saved_models/classifier_diabetes', 'wb') as file:
    pickle.dump(classifier, file)

# Using the saved model for future predictions

with open('assets/saved_models/classifier_diabetes', 'rb') as file:
    new_classifier = pickle.load(file)

y_pred_from_saved_model = new_classifier.predict(X_test)

print("Accuracy from saved model:", metrics.accuracy_score(y_test, y_pred_from_saved_model))

# create desktop application and use saved model
